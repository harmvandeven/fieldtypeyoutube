<?php

namespace ProcessWire;

class YouTubeData extends WireData
{

  function __construct()
  {
    $this->clear();
  }

  public function clear()
  {
    $this->set('id', null);
    $this->set('name', null);
    $this->set('description', null);
    $this->set('link', null);
    $this->set('dimension', null);
    $this->set('definition', null);
    $this->set('ratio', null);
    $this->set('duration', null);
    $this->set('pictures', null);
    $this->set('dates', null);
    $this->set('embed', null);
    $this->set('channel_id', null);
    $this->set('channel_title', null);
  }

  public function isValid()
  {
    return ($this->get('link') == '' ? false : true);
  }

  public function set($key, $value)
  {
    return parent::set($key, $value);
  }

  public function get($key)
  {
    if ($key == 'statusString') return $this->get('id');
    return parent::get($key);
  }

  public function __toString()
  {
    if (!$this->isValid()) {
      return 'No valid YouTube link selected';
    }
    return $this->get('link') . ' —-- ' . $this->get('name');
  }
}
